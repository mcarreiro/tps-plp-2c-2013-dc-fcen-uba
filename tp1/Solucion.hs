module Solucion where

import List
import Tipos
import Generador

data Melodia = Silencio Duracion
		 | Nota Tono Duracion
		 | Secuencia Melodia Melodia
                 | Paralelo [Melodia]
  deriving (Show, Eq)

-- Funciones auxiliares

foldNat::a->(a->a)->Integer->a
foldNat caso0 casoSuc n | n == 0 = caso0
			| n > 0 = casoSuc (foldNat caso0 casoSuc (n-1))
			| otherwise = error "El argumento de foldNat no puede ser negativo."

maxLista::(Ord a)=>[a]->a
maxLista = foldr1 max

fix::(a->a)->a
fix f = f (fix f)

--fRec::Melodia->Melodia
--fRec = (\x -> Paralelo [m, Secuencia (Silencio d) x])
-- Habría que ver como definir esa funcion frec para canon y canon Infinito
-- Funciones pedidas

-- Bastante claro, para reproducir las dos sin detener la primera es necesario hacer un Paralelo
-- Y la Secuencia es para la duracion de la espera
superponer::Melodia->Duracion->Melodia->Melodia
superponer mel1 espera mel2 = Paralelo [mel1,Secuencia (Silencio espera) mel2]

--Sugerencia: usar foldNat
--En caso de rep == 0 devuelve un Silencio con duracion d
--En otro caso rep-1 veces construye el canon de la melodia poniendola en Paralelo con la melodia y un silencio de duracion d secuenciada de la melodiass
canon::Duracion->Integer->Melodia->Melodia
canon d rep m = if rep == 0 then 
					Silencio d 
				else 
					foldNat m fRec (rep-1) where fRec = superponer m d

-- Une las melodias 'concatenandolas' en Secuencia
secuenciaLista :: [Melodia] -> Melodia--Se asume que la lista no es vacía.
secuenciaLista listMel = foldr1 Secuencia listMel

canonInfinito::Duracion->Melodia->Melodia
canonInfinito d m = fix (superponer m d)
--Basicamente lo que hace es lo mismo que canon, pero mel nunca llega a terminar al final, pero va air cantando infinitamente

repeticionInfinita :: Melodia -> Melodia
repeticionInfinita melodia = fix (Secuencia melodia)
--Mel no puede ser primero ya que es el de la recursion, por lo qu enunca empezaría nada
--Estaria esperando a que termine la recursion, lo caul nunca va a hacer

-- Esquema de recursion fold para el tipo Melodia
-- A cada parte de cada constructor se le aplica una funcion pasada por parametro dependiendo el constructor
foldMelodia::(Duracion->b)->(Tono->Duracion->b)->(b->b->b)->([b]->b)->Melodia->b
foldMelodia fSilencio fNota fSecuencia fParalelo melo = case melo of
	Silencio d -> fSilencio d
	Nota t d -> fNota t d
	Secuencia m1 m2 -> fSecuencia (rec m1) (rec m2)
	Paralelo ms -> fParalelo (map rec ms) 
	where rec = foldMelodia fSilencio fNota fSecuencia fParalelo

-- Basicamente usando foldMelodia deja todo com esta salvo que sea una Nota
-- En ese caso le aplica la funcion que transforma el Tono de la misma				      
mapMelodia :: (Tono -> Tono)->Melodia->Melodia
mapMelodia f = foldMelodia Silencio  fNota Secuencia Paralelo
	where fNota = (\t -> Nota (f t) )

-- transportar usa mapMelodia que a su vez usa foldMelodia, ya que mapeaba la suma de n semitonos a cada nota
transportar :: Integer -> Melodia -> Melodia
transportar n = mapMelodia (+n)

-- con foldMelodia para silencio y nota les cambia la duracion multiplicandola
cambiarVelocidad::Float->Melodia->Melodia--Sugerencia: usar round y fromIntegral
cambiarVelocidad factor = foldMelodia fSilencio fNota Secuencia Paralelo
	where 
		fSilencio d = Silencio (multiplyRounded factor d)
		fNota t d = Nota t (multiplyRounded factor d)

multiplyRounded::Float->Integer->Integer
multiplyRounded f d = round (f*(fromInteger d))

-- invertir:
-- Si no es secuencia la deja como esta porque no hay nada para invertir
-- Y en caso de ser secuencia alterna las dos melodias
invertir :: Melodia -> Melodia
invertir = foldMelodia Silencio Nota fSecuencia Paralelo
	where 
		fSecuencia = flip Secuencia

-- duracionTotal:
-- Mide la duracion total, es decir, en paralelo se queda con la de mayor duracion y en secuencia las suma.
-- Silencio y Nota tiene una duracion propia.
duracionTotal::Melodia->Duracion
duracionTotal = foldMelodia fSilencio fNota fSecuencia fParalelo
	where 
		fSilencio = id 
		fNota t = id
		fSecuencia = (+)
		fParalelo = maximum
						       
--EJERCICIO 5
-- En instantes menores que 0 no suena ninguna nota. Se puede usar recursión explícita. Resaltar las partes del código que hacen que no se ajuste al esquema fold.
--  NO SE AJUSTA A FOLD principalmente por el constructor de secuencia y la funcion que se le aplica, ya que esta recibe como parametros las dos partes de la secuencia y no el paso recursivo como sucede en los fold. Además a cada parte de la secuencia se le aplica una operacion distinta.s
notasQueSuenan::Instante->Melodia->[Tono]
notasQueSuenan instant melodia | instant < 0 = []
	| instant >= 0 = case melodia of
	Silencio d -> []
	Nota t d -> fNotaQueSuenaNota d instant t
	Paralelo ms -> (concatMap segui ms) 
	Secuencia m1 m2 -> (notasQueSuenan instant m1) ++ (notasQueSuenan (instant - (duracionTotal(m1))) m2)
	where 
	segui = notasQueSuenan instant

fNotaQueSuenaNota::Instante->Duracion->Tono->[Tono]
fNotaQueSuenaNota instant d t | d >= instant = []
				  | d < instant = [t]				  

				 
				  
--FIN EJERCICIO 5


--EJERCICIO 6

--A)
-- Tal como dice el enunciado, usando listas por compresion y sin repetir ponemos Off en las que estan en la primera
-- y no en la segunda, y On en las que estan en la segunda y no en la primera.
cambios :: Instante->[Tono]->[Tono]->[Evento]--Sugerencia: usar listas por comprensión. No repetir eventos.
cambios i l1 l2 = nub ([Off i x | x<-l1, not (x `elem` l2)]++[On i x | x<- l2, not (x `elem` l1)] )

--B)
-- Hago llamados recursivos de 0 al instante solicitado con foldl (utilizado una lista por compresión con todos los instantes) 
-- y mediante la funcion "cambios" entre el instante anterior y el acutal voy generando que notas encender o apagar

eventosPorNotas::(Instante->[Tono])->Duracion->[Evento]
eventosPorNotas getToneByMoment duration = (foldl recTones firstTone [1 .. duration]) ++ (cambios (duration+1) (getToneByMoment duration) ([]))
	where 
		firstTone = cambios 0 ([]) (getToneByMoment 0) 
		recTones = (\lastTones instant ->(lastTones) ++ (cambios instant (getToneByMoment (instant-1)) (getToneByMoment instant)))
		
--C)
-- Esta función es bastante intuitiva ya que "flip notasQueSuenan m" es una función que toma un instante y devulve los notos que hubo en ese instante, en la melodía dada
-- Por lo que solo queda obtener los eventos de esas notas, y la función anterior nos otorga esto dada la duración
eventos :: Melodia -> Duracion -> [Evento]		
eventos m d = eventosPorNotas (flip notasQueSuenan m) d





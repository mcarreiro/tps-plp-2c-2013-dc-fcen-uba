module Tipos where

type Tono         = Integer
type Duracion     = Integer
type Instante     = Integer
data Evento       = On Instante Tono | Off Instante Tono
  deriving (Show, Eq)

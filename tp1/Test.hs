import Tipos
import Generador
import Solucion
import HUnit


--Notas para pruebas.

_sol0 = Nota 55
_si0  = Nota 59
_do = Nota 60
_reb  = Nota 61
_re = Nota 62
_mib  = Nota 63
_mi = Nota 64
_fa = Nota 65
_fas  = Nota 66
_sol = Nota 67
_lab  = Nota 68
_la = Nota 69
_sib  = Nota 70
_si = Nota 71
_do2  = Nota 72
_reb2 = Nota 73
_re2  = Nota 74
_mib2 = Nota 75
_fa2  = Nota 77

-- Melodías para pruebas.

acorde::Melodia
acorde = Paralelo [_do 10, Secuencia (Silencio 3) (_mi 7), Secuencia (Silencio 6) (_sol 4)]

doremi::Melodia
doremi = secuenciaLista [_do 3, _re 1, _mi 3, _do 1, _mi 2, _do 2, _mi 4]

 --Pongan sus propias pruebas y melodías. Pueden definir más notas, la numeración es por semitonos.

-- Canon APL (autor: Pablo Barenbaum)

rhoRhoRhoOfX, alwaysEqualsOne, rhoIsDimensionRhoRhoRank, aplIsFun :: Melodia
rhoRhoRhoOfX = secuenciaLista $ map (\(d, f)->f d) [(4, _do), (4, _do), (3, _do), (1, _re), (4, _mi)]
alwaysEqualsOne = secuenciaLista $ map (\(d, f)->f d) [(3, _mi), (1, _re), (3, _mi), (1, _fa), (8, _sol)]
rhoIsDimensionRhoRhoRank = secuenciaLista $ map (\(d, f)->f d) [(12, _do2), (12, _sol), (12, _mi), (12, _do)]
aplIsFun = secuenciaLista $ map (\(d, f)->f d) [(3, _sol), (1, _fa), (3, _mi), (1, _re), (8, _do)]

mezcla :: Melodia
mezcla = Paralelo [rhoRhoRhoOfX, Secuencia (Silencio 4) alwaysEqualsOne, Secuencia (Silencio 8) rhoIsDimensionRhoRhoRank, Secuencia (Silencio 12) aplIsFun]

-- Cangrejo (autor: Pablo Barenbaum)

stac :: Tono -> Melodia
stac t = Secuencia (Nota t 9) (Silencio 1)

stacatto :: Melodia -> Melodia
stacatto = foldMelodia Silencio (\t d->stac t) Secuencia Paralelo

cangrejo1 = secuenciaLista $ 
	       [Silencio 4, _do 2, _mib 2]
	    ++ [_sol 2, _lab 4, Silencio 2]
	    ++ [_si0 4, Silencio 2, _sol 4] 
	    ++ [_fas 4, _fa 4]              
	    ++ [_mi 2, Silencio 2, _mib 4]  
	    ++ [_re 2, _reb 2, _do 2]
	    ++ [_si0 2, _sol0 2, _do 2, _fa 2]
	    ++ [_mib 2, _re 4, Silencio 2]
	    ++ [_do 2, _mi 2, Silencio 4]
cangrejo2 = secuenciaLista $ (map (\(d, f)->f d)) $
               [(2, _do), (2, _mib), (2, _sol), (2, _do2)]
            ++ [(1, _sib), (1, _do2), (1, _re2), (1, _mib2),
                (1, _fa2), (1, _mib2), (1, _re2), (1, _do2)]
            ++ [(1, _re2), (1, _sol), (1, _re2), (1, _fa2),
                (1, _mib2), (1, _re2), (1, _do2), (1, _si)]
            ++ [(1, _la), (1, _si), (1, _do2), (1, _mib2),
                (1, _re2), (1, _do2), (1, _si), (1, _la)]
            ++ [(1, _sol), (1, _lab), (1, _sib), (1, _reb2),
                (1, _do2), (1, _sib), (1, _lab), (1, _sol)]
            ++ [(1, _fa), (1, _sol), (1, _lab), (1, _sib),
                (1, _lab), (1, _sol), (1, _fa), (1, _mib)]
            ++ [(1, _re), (1, _mib), (1, _fa), (1, _sol),
                (1, _fa), (1, _mib), (1, _re), (1, _lab)]
            ++ [(1, _sol), (1, _fa), (1, _mib), (1, _do2),
                (1, _si), (1, _la), (1, _sol), (1, _fa)]
            ++ [(1, _mi), (1, _re), (1, _mi), (1, _sol),
                (1, _do2), (1, _sol), (1, _fa), (1, _sol)]
                
cangrejo = Secuencia c (invertir c)
  where c = Paralelo [cangrejo1, cangrejo2]

primavera  = secuenciaLista [_sol 1, _si 1,Silencio 1, _si 1,Silencio 1, _si 1, _la 1, _sol 1, _re2 2, Silencio 3,_re2 1, _do2 1, _si 1,Silencio 1, _si 1,Silencio 1, _si 1, _la 1, _sol 1, _re2 2 ]
paraElisa = secuenciaLista [_mi 1, _re2 1, _mi 1, _re2 1, _mi 1, _si 1, _re2 1, _do2 1, _la 1,Silencio 4, _do2 1, _mi 1, _la 1, _si 2,Silencio 4, _mi 1,_sol 1, _si 1, _do 1]
mariposaTeknicolor = (secuenciaLista [_sol 1,Silencio 1,_sol 1,Silencio 1,_sol 1,Silencio 1,_sol 1,Silencio 1,_sol 1,_fa 1,_mi 1,_fa 1, _sol 1])
mariposaTeknicolorAlReves = (secuenciaLista [_sol 1, _fa 1,_mi 1, _fa 1,_sol 1 ,Silencio 1, _sol 1,Silencio 1,_sol 1,Silencio 1,_sol 1,Silencio 1,_sol 1 ])
--

genMelodia :: String -> Melodia -> Duracion -> IO ()
genMelodia fn m dur = generarMidi fn (eventos m dur)


main = do
	

   putStr "Generando apl-is-fun.mid...\n"
   genMelodia "apl-is-fun.mid" (stacatto mezcla) 500
   putStr "Generando cangrejo.mid...\n"
   genMelodia "cangrejo.mid" (stacatto cangrejo) 1000
  
   genMelodia "primavera.mid" (stacatto primavera) 500

   genMelodia "paraelisa.mid" (stacatto paraElisa) 500
   genMelodia "mariposa.mid" (stacatto mariposaTeknicolor) 1000
   
   putStr "HUnit Tests:\n"
   putStr "\nEjercicio 1a\n"
   runTestTT tests1a
   putStr "\nEjercicio 1b\n"
   runTestTT tests1b
   putStr "\nEjercicio 1c\n"
   runTestTT tests1c
   putStr "\nEjercicio 4b (usa el 4a, que a su vez usa el 3)\n"
   runTestTT tests4b
   putStr "\nEjercicio 4c (usa el 3)\n"
   runTestTT tests4c
   putStr "\nEjercicio 4d (usa el 3)\n"
   runTestTT tests4d
   putStr "\nEjercicio 4e (usa el 3)\n"
   runTestTT tests4e
   putStr "\nEjercicio 5\n"
   runTestTT tests5
   putStr "\nEjercicio 6a\n"
   runTestTT tests6a
   putStr "\nEjercicio 6b\n"
   runTestTT tests6b
   putStr "\nEjercicio 6c\n"
   runTestTT tests6c
   
test1a1 = TestCase(assertEqual "" (superponer (Nota 60 1) 10 (Secuencia (Silencio 1) (Nota 30 1))) (Paralelo [Nota 60 1,Secuencia (Silencio 10) (Secuencia (Silencio 1) (Nota 30 1))]))
tests1a = TestList [ test1a1]

test1b1 = TestCase(assertEqual "ejemplo" (canon 2 3 (Nota 60 4)) (Paralelo [Nota 60 4,Secuencia (Silencio 2) (Paralelo [Nota 60 4,Secuencia (Silencio 2) (Nota 60 4)])]))
test1b2 = TestCase(assertEqual "repitiendo 1 da lo mismo" (canon 2 1 (Nota 60 4)) (Nota 60 4))
test1b3 = TestCase(assertEqual "repitiendo 0 da vacio" (canon 2 0 (Nota 60 4)) (Silencio 2))
tests1b = TestList [ test1b1,test1b2,test1b3]

test1c1 = TestCase(assertEqual "" (secuenciaLista [Nota 60 4,Nota 60 4]) (Secuencia (Nota 60 4) (Nota 60 4)))
test1c2 = TestCase(assertEqual "" (secuenciaLista [Secuencia (Nota 60 4) (Nota 60 4),Secuencia (Nota 60 4) (Nota 60 4)]) (Secuencia (Secuencia (Nota 60 4) (Nota 60 4)) (Secuencia (Nota 60 4) (Nota 60 4))))
tests1c = TestList [ test1c1,test1c2]

test4b1 = TestCase (assertEqual "en cero queda igual" (transportar 0 (Nota 10 2)) (Nota 10 2))
test4b2= TestCase (assertEqual "le sumo un numero" (transportar 10 (Nota 10 2)) (Nota 20 2))
test4b3= TestCase (assertEqual "mas complejo, las notas deberian quedar en 70" (transportar 10 (canon 2 3 (Nota 60 4))) (Paralelo [Nota 70 4,Secuencia (Silencio 2) (Paralelo [Nota 70 4,Secuencia (Silencio 2) (Nota 70 4)])]))
tests4b = TestList[test4b1,test4b2,test4b3]

test4c1 = TestCase(assertEqual "" (duracionTotal (superponer (Nota 60 1) 10 (Secuencia (Silencio 1) (Nota 30 1)))) (12))
test4c2 = TestCase(assertEqual "" (duracionTotal (Secuencia (Silencio 1) (Nota 30 1))) (2))
tests4c = TestList [ test4c1,test4c2]

test4d1 = TestCase (assertEqual "en unoqueda igual" (cambiarVelocidad 1 (Nota 10 2)) (Nota 10 2))
test4d2= TestCase (assertEqual "por 10" (cambiarVelocidad 10 (Nota 10 2)) (Nota 10 20))
test4d3= TestCase (assertEqual "mas complejo, las notas deberian quedar en 8 y silencios en 4" (cambiarVelocidad 2 (canon 2 3 (Nota 60 4))) (Paralelo [Nota 60 8,Secuencia (Silencio 4) (Paralelo [Nota 60 8,Secuencia (Silencio 4) (Nota 60 8)])]))
tests4d = TestList[test4d1,test4d2,test4d3]


test4e1 = TestCase (assertEqual "si no es seq ni paralelo queda igual" (invertir (Nota 10 2)) (Nota 10 2))
test4e2 = TestCase (assertEqual "invertir dos veces es igual a la original" (invertir (invertir (canon 2 3 (Nota 60 4)))) (canon 2 3 (Nota 60 4)))
test4e3 = TestCase (assertEqual "paralelas sin secuencias quedan igual" (invertir (Paralelo [(Nota 10 2) ,(Silencio 3)])) ((Paralelo [(Nota 10 2) ,(Silencio 3)])))
test4e4 = TestCase (assertEqual "invertir dos veces es igual a la original" (invertir (invertir (mariposaTeknicolor))) (mariposaTeknicolor))
test4e5 = TestCase (assertEqual "si no es seq ni paralelo queda igual" (invertir (Paralelo [Secuencia (Nota 10 10) (Silencio 1)])) (Paralelo [Secuencia (Silencio 1) (Nota 10 10)]))
tests4e = TestList[test4e1,test4e2, test4e3 ,test4e4,test4e5]

test5_1 = TestCase (assertEqual "instante 0 de una nota da el tono" (notasQueSuenan 0 (Nota 10 2)) ([10]))
test5_2 = TestCase (assertEqual "tono de silencio tiene que dar vacio" (notasQueSuenan 0 (Silencio 2)) ([]))
test5_3 = TestCase (assertEqual "" (notasQueSuenan 0 (Secuencia (Silencio 2) (Nota 70 4))) ([]))
test5_4 = TestCase (assertEqual "" (notasQueSuenan 1 (Secuencia (Silencio 2) (Nota 70 4))) ([]))
test5_5 = TestCase (assertEqual "" (notasQueSuenan 2 (Secuencia (Silencio 2) (Nota 70 4))) ([70]))
test5_6 = TestCase (assertEqual "" (notasQueSuenan 3 (Secuencia (Silencio 2) (Nota 70 4))) ([70]))
test5_7 = TestCase (assertEqual "" (notasQueSuenan 4 (Secuencia (Silencio 2) (Nota 70 4))) ([70]))
test5_8 = TestCase (assertEqual "" (notasQueSuenan 5 (Secuencia (Silencio 2) (Nota 70 4))) ([70]))
test5_9 = TestCase (assertEqual "" (notasQueSuenan 6 (Secuencia (Silencio 2) (Nota 70 4))) ([]))
test5_10 = TestCase (assertEqual "" (notasQueSuenan 7 (Secuencia (Silencio 2) (Nota 70 4))) ([]))
tests5 = TestList[test5_1,test5_2,test5_3,test5_4,test5_5,test5_6,test5_7,test5_8,test5_9,test5_10]

test6a1 = TestCase (assertEqual "cambios 1 [1,2,3,4,5] [1,2,7,5,7,4,9]," (cambios 1 [1,2,3,4,5] [1,2,7,5,7,4,9]) ([Off 1 3,On 1 7,On 1 9]))
test6a2 = TestCase (assertEqual "cambios 3 [7,9] [1,2,7,5,7,4,9]" (cambios 3 [7,9] [1,2,7,5,7,4,9]) ([On 3 1,On 3 2,On 3 5,On 3 4]))
test6a3 = TestCase (assertEqual "primera vacia, todos en ON" (cambios 3 [] [1,2,7,5,7,4,9]) ([On 3 1,On 3 2,On 3 7,On 3 5,On 3 4,On 3 9]))
test6a4 = TestCase (assertEqual "segunda vacia, todos en OFF" (cambios 3 [1,2,7,5,7,4,9] [])  ([Off 3 1,Off 3 2,Off 3 7,Off 3 5,Off 3 4,Off 3 9]))
test6a5 = TestCase (assertEqual "vacias -> vacia" (cambios 3 [] []) ([]))
tests6a = TestList [ test6a1, test6a2, test6a3, test6a4, test6a5]   


funcionEjemplo6b::Integer-> [Tono]
funcionEjemplo6b i | i == 0 = [60]
	| i == 1 = [60,64]
	| i == 2 = []
	| i == 3 = [67]
	| i == 4 = []

funcionAnteriores6b::Integer->[Tono]
funcionAnteriores6b 0  = []
funcionAnteriores6b (x+1) = [x+1] ++ funcionAnteriores6b x

generarTonos x = map (\y -> y*x) (funcionAnteriores6b x)
-- generarTonos 1 -> [1]
-- generarTonos 2 -> [4,2]
-- generarTonos 3 -> [9,6,3]

	
test6b1 = TestCase (assertEqual "eventosPorNotas funcionEjemplo6b 2," ([On 0 60,On 1 64,Off 2 60,Off 2 64]) (eventosPorNotas funcionEjemplo6b 2))
test6b2 = TestCase (assertEqual "eventosPorNotas funcionEjemplo6b 3," ([On 0 60,On 1 64,Off 2 60,Off 2 64,On 3 67,Off 4 67]) (eventosPorNotas funcionEjemplo6b 3))
test6b3 = TestCase (assertEqual "" (eventosPorNotas generarTonos 3) ([On 1 1,Off 2 1,On 2 4,On 2 2,Off 3 4,Off 3 2,On 3 9,On 3 6,On 3 3,Off 4 9,Off 4 6,Off 4 3]))
test6b4 = TestCase (assertEqual "" (eventosPorNotas (\x->[]) 100) ([]))
tests6b = TestList [test6b1, test6b2, test6b3, test6b4]


fasolla::Melodia
fasolla = secuenciaLista [Paralelo[_fa 3,_sol 4, _la 5], _sol 3, Silencio 1, _fa 3, _la 1, _fa 2]


tests6c = TestList [ test6c1, test6c2, test6c3, test6c4] 
test6c1 = TestCase (assertEqual "eventos acorde 6," ([On 0 60,On 3 64,On 6 67,Off 7 60, Off 7 64, Off 7 67]) (eventos acorde 6))
test6c2 = TestCase (assertEqual "eventos doremi 5," ([On 0 60,Off 3 60,On 3 62,Off 4 62, On 4 64, Off 6 64]) (eventos doremi 5))
test6c3 = TestCase (assertEqual "eventos fasolla 5," ([On 0 65,On 0 67,On 0 69,Off 3 65, Off 4 67, Off 5 69, On 5 67, Off 6 67]) (eventos fasolla 5))
test6c4 = TestCase (assertEqual "eventos fasolla 8," ([On 0 65,On 0 67,On 0 69,Off 3 65, Off 4 67, Off 5 69, On 5 67, Off 8 67]) (eventos fasolla 8))

module Midi.IOGhc(openBinaryOutputFile) where

import IO

openBinaryOutputFile :: String -> IO Handle
openBinaryOutputFile fn = openFile fn WriteMode


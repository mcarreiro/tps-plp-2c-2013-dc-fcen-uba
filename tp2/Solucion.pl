teclado([ (1, [1]), (2, [2,a,b,c]), (3, [3,d,e,f]),
(4, [4,g,h,i]), (5, [5,j,k,l]), (6, [6,m,o,n]),
(7, [7,p,q,r,s]), (8, [8,t,u,v]), (9, [9,w,x,y,z]),
(0, [0]), (*, [-]), (#, [#])
]).

diccionario([ [1,a], [l,a], [c,a,s,a], [a], [d,e], [r,e,j,a], [t,i,e,n,e],
[c,a,s,a,m,i,e,n,t,o], [d,e,l], [a,n,t,e,s], [w,a,l,t,e,r]
]).

teclasNecesarias([],[]).
teclasNecesarias([L|X],[N|Y]):- tecla(L,N), teclasNecesarias(X,Y).

tecla(X, B) :- teclado(C),member(L,C), valor(L,A), member(X,A), clave(L,B). 

clave((X,C),X).
valor((X,C),C).

palabraPosible(T,P):- diccionario(D), member(P,D), prefijo(T,P).

prefijo([],_).
prefijo([N|D], [L|P]):- prefijo(D,P), tecla(L,N).

isSubset([],_).
isSubset([H|T],Y):-
    member(H,Y), %El head esta en Y
    select(H,Y,Z), %Formo Z como, Y - head, esto es porque queremos obviar el orden
    isSubset(T,Z). %Veo que T sea subset de este Z
equal(X,Y):- %La igualdad de conjuntos es la inclusión para ambos lados
    isSubset(X,Y),
    isSubset(Y,X).

todasLasPalabrasPosibles(Ds,Ps):- setof(P, (palabraPosible(Ds,P)) ,As), equal(Ps,As), !.


oracionPosible(A, B) :- implode(A,*, AS), oracionPosibleListaDeListas(AS,BS), flatten(BS,B).
oracionPosible([], []).

oracionPosibleListaDeListas([AS], [B]) :- palabraPosible(AS, B).
oracionPosibleListaDeListas([AS|XSS], [B|[-|WS]]) :- palabraPosible(AS, B), oracionPosibleListaDeListas(XSS, WS).

implode([], _, []).
implode(XS, C, [XS]) :- XS \= [], not(member(C,XS)).
implode(XS, C, [LP|YSS]) :- append(LP,[C|LS],XS), not(member(C,LP)), implode(LS,C,YSS).
 
  %----------------------------------TESTS-------------------------------------%
%----------------------------------------------------------------------------%

:- begin_tests(lists).
:- use_module(library(lists)).
:- use_module(library(plunit)).
:- use_module(library(debug)).
% En este caso probamos que ante una palabra vacía, se devuelva una lista de dígitos vacía.
test(tN_PalabraVacia) :-        
               teclasNecesarias([], Ds),
               Ds == [].

% Probamos que teclasNecesarias devuelva true con ambas listas vacías (es un hecho).
%-------------------------teclasNecesarias--------------------------------------%
test(tN_AmbasVacias) :-        
               teclasNecesarias([],[]).
test(tN_AmbasVacias, [nondet]) :-        
               teclasNecesarias([c,a,s,a],P),
			   Res = [[2,2,7,2]],
			   member(P,Res).			   

% Palabra posible ejemplo
%-------------------------palabraPosible--------------------------------------%
test(pP_Enunciado, [nondet]) :-
       palabraPosible([2], Pal),
       Res = [ [a], [a, n, t, e, s], [c, a, s, a], [c, a, s, a, m, i, e, n, t, o] ],
       member(Pal, Res).	
test(tN_AmbasVaciasPP, [nondet]) :-        
               palabraPosible([], O),
			    diccionario(Res),
               member(O,Res).  

test(pP_ejemplo1,[nondet]) :-  palabraPosible([2], [a]).
test(pP_ejemplo2,[nondet]) :-  palabraPosible([2], [a, n, t, e, s]).
test(pP_ejemplo3,[nondet]) :-  palabraPosible([2], [c, a, s, a]).
test(pP_ejemplo4,[nondet]) :-  palabraPosible([2], [c, a, s, a, m, i, e, n, t, o]).

%-------------------------todasLasPalabrasPosibles--------------------------------------%
test(tPP_ejemplo1) :- todasLasPalabrasPosibles([2], P), P == [[a], [a, n, t, e, s], [c, a, s, a], [c, a, s, a, m, i, e, n, t, o]].
test(tPP_ejemplo2) :- todasLasPalabrasPosibles([2], [[a], [a, n, t, e, s], [c, a, s, a], [c, a, s, a, m, i, e, n, t, o]]).

%-------------------------oracionPosible--------------------------------------%

test(tN_OracionVacia) :-        
               oracionPosible([], Ds),
               Ds == [].

			   
test(tN_Oracion2, [nondet]) :-        
               oracionPosible([7,3,*,3], O),
			    Res = [[r, e, j, a, -, d, e], [r, e, j, a, -, d, e, l] ],
               member(O,Res).
			   
test(tN_Oracion3, [nondet]) :-        
               oracionPosible([7,*,9], O),
			    Res = [[r, e, j, a, -, w, a, l, t, e, r]],
               member(O,Res).
%oracionPosible de una sola teclar deberia ser igual que palabraPosible con ese caracter			   
test(tN_Oracion4, [nondet]) :-        
               oracionPosible([2], O),
			    Res = [[a], [a, n, t, e, s], [c, a, s, a], [c, a, s, a, m, i, e, n, t, o]],
               member(O,Res).			   
		   
test(tN_Oracion5, [nondet]) :-        
               oracionPosible([2,*,3], O),
			    Res = [[a, -, d, e], [a, -, d, e, l], [a, n, t, e, s, -, d, e], [a, n, t, e, s, -, d, e, l], [c, a, s, a, m, i, e, n, t, o, -, d, e], [c, a, s, a, m, i, e, n, t, o, -, d, e, l]],
               member(O,Res).
			   

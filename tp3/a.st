TestCase subclass: #TransformadorTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransformadorTest methodsFor: 'testsBasicos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransBloque

	| trans5 unBloque unaColeccionOrdenada |
	trans5 := self transformador5 .
	unaColeccionOrdenada := OrderedCollection new.
	unBloque := [ unaColeccionOrdenada add: 0 ].
	
	(trans5 transform: unBloque) value.
	self assert: ( unaColeccionOrdenada size = 3 ).! !

!TransformadorTest methodsFor: 'testsBasicos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransCaracter

	| trans2 trans3 |
	trans2 := self transformador2 .
	trans3 := self transformador3 .
	
	self assert: ( (trans2 transform: 'patoloco') = 'pbtoloco' ).
	self assert: ( (trans2 transform: 'monoloco') = 'monoloco' ).
	self assert: ( (trans3 transform: 'elosodebimbo') = 'elosodeaimao' ).
	self assert: ( (trans3 transform: 'patoloco') = 'patoloco' ).
! !

!TransformadorTest methodsFor: 'testsBasicos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransIdentidad

| unTransformadorIdentidad |
unTransformadorIdentidad := self transformador1 .
self assert: ( unTransformadorIdentidad transform: self == self ).! !

!TransformadorTest methodsFor: 'testsBasicos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransSubstring

	| trans4 |
	trans4 := self transformador4 .
	
	self assert: ( (trans4 transform: 'monoloco') = 'monoloco' ).
	self assert: ( (trans4 transform: 'buu') = 'md' ).
	self assert: ( (trans4 transform: 'monobuuloco') = 'monomdloco' ).! !


!TransformadorTest methodsFor: 'testsCompuestos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransCondicion

	| trans8 trans9 
	unStringImpar otroStringImpar unStringPar otroStringPar 
	unBloquePar unBloqueImpar unaColeccionOrdenada unNuevoBloque |
	
	trans8 := self transformador8 .
	trans9 := self transformador9 .
	
	unStringImpar := 'bab'.
	unStringPar := 'aa'.
	otroStringImpar := 'aaa'.
	otroStringPar := 'bb'.
	
	self assert: ( (trans8 transform: unStringPar) = 'bb' ).
	self assert: ( (trans8 transform: unStringImpar) = 'aaa' ).
	self assert: ( (trans8 transform: otroStringPar) = otroStringPar ).
	self assert: ( (trans8 transform: otroStringImpar ) = otroStringImpar ).
	
	unaColeccionOrdenada := OrderedCollection new.
	unBloquePar := [ unaColeccionOrdenada add:0. ^4 ].
	unBloqueImpar := [ unaColeccionOrdenada add:0. ^3 ].
	
	unNuevoBloque := trans9 transform: unBloquePar.
	unaColeccionOrdenada := OrderedCollection new.
	unNuevoBloque value.
	self assert: (unaColeccionOrdenada size = 3).
	
	unNuevoBloque := trans9 transform: unBloqueImpar.
	unaColeccionOrdenada := OrderedCollection new.
	unNuevoBloque value.
	self assert: (unaColeccionOrdenada size = 1).! !

!TransformadorTest methodsFor: 'testsCompuestos' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testTransSecuencial

	| trans6 trans7 |
	trans6 := self transformador6 .
	trans7 := self transformador7 .
	
	self assert: ( (trans6 transform: 'patoloco') = 'patoloco' ).
	self assert: ( (trans6 transform: 'monoloco') = 'monoloco' ).
	self assert: ( (trans6 transform: 'elosodebimbo') = 'elosodeaimao' ).
	self assert: ( (trans7 transform: 'patoloco') = 'pbtoloco' ).
	self assert: ( (trans7 transform: 'monoloco') = 'monoloco' ).! !


!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'MartinCarreiro 11/11/2013 16:07'!
transformador1
	"Debe retornar el transformador especificado en el enunciado"
	^TransIdentidad new.! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'MartinCarreiro 12/10/2013 13:11'!
transformador2
	"Debe retornar el transformador especificado en el enunciado"
	^TransCaracter newWith:$a and:$b! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'MartinCarreiro 12/10/2013 13:11'!
transformador3
	"Debe retornar el transformador especificado en el enunciado"
	^TransCaracter newWith:$b and:$a! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'MartinCarreiro 12/10/2013 13:12'!
transformador4
	"Debe retornar el transformador especificado en el enunciado"
	^TransSubstring newWith:'buu' and:'md'! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'MartinCarreiro 11/11/2013 16:54'!
transformador5
	"Debe retornar el transformador especificado en el enunciado"
	^TransBloque newWith:3.! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'KevinKujawski 11/14/2013 00:17'!
transformador6
	"Debe retornar el transformador especificado en el enunciado"
	^TransSecuencial newWith:(self transformador2) and:(self transformador3) ! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'KevinKujawski 11/14/2013 00:17'!
transformador7
	"Debe retornar el transformador especificado en el enunciado"
	^TransSecuencial newWith:(self transformador6) and:(self transformador2) ! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'KevinKujawski 11/14/2013 00:32'!
transformador8
	
	"Debe retornar el transformador especificado en el enunciado"
	^TransCondicion newWith:  [:aString | aString size even ] going: (self transformador2) or: (self transformador3)! !

!TransformadorTest methodsFor: 'transformadoresDefinidos' stamp: 'KevinKujawski 11/14/2013 00:49'!
transformador9
	"Debe retornar el transformador especificado en el enunciad o"
	^TransCondicion newWith:  [:aBlock | aBlock value even ] going: (self transformador5) or: (self transformador1)! !


Object subclass: #Internacionalizador
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

Internacionalizador class
	instanceVariableNames: ''!

!Internacionalizador class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 12:52'!
renombrar: selectorOriginal por: nuevoSelector
 | refactoring |
	refactoring := RBRenameMethodRefactoring 
		renameMethod: selectorOriginal 
		in: Transformador
		to: nuevoSelector
		permutation: (1 to: 1).
		refactoring execute .! !


TestCase subclass: #InternacionalizadorTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!InternacionalizadorTest methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:30'!
testRenameAMethodWithANameThatExists
	"Al renombrar un metodo con un nombre que ya existe deberia explotar"
	|newSelector oldSelector|
	newSelector :=('initializeWith',':') asSymbol .
	oldSelector :=('transformar',':') asSymbol .
	self should: [Internacionalizador renombrar: oldSelector por: newSelector] raise: Error.
	! !

!InternacionalizadorTest methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:30'!
testRenameANonExistMethod
	"Al renombrar un metodo que no existe deberia explotar"
	|newSelector oldSelector|
	newSelector :=('transform',':') asSymbol .
	oldSelector :=('metodoNoExistis',':') asSymbol .
	self should: [Internacionalizador renombrar: oldSelector por: newSelector] raise: Error.
	! !

!InternacionalizadorTest methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:31'!
testRenameMethod
	
	"Usarlo bajo su propia responsabilidad (recargar imagen para asegurarse que la casa esta en orden luego de usar)"
	|newSelector oldSelector|
	newSelector :=('transform',':') asSymbol .
	oldSelector :=('transformar',':') asSymbol .
	Internacionalizador renombrar: oldSelector por: newSelector.

	! !

!InternacionalizadorTest methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
testRenameMethodPrueba
	
	"Usarlo bajo su propia responsabilidad (recargar imagen para asegurarse que la casa esta en orden luego de usar)"
	|newSelector oldSelector|
	newSelector :=('modify',':') asSymbol .
	oldSelector :=('modificar',':') asSymbol .
	Internacionalizador renombrar: oldSelector por: newSelector.

	! !


Object subclass: #Transformador
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!Transformador methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform: unObjeto
	self subclassResponsibility  .! !


Transformador subclass: #TransCompuesto
	instanceVariableNames: 't1 t2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransCompuesto methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 11/11/2013 16:57'!
initializeWith: newt1 and: newt2

	t1 := newt1.
	t2 := newt2.	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

TransCompuesto class
	instanceVariableNames: ''!

!TransCompuesto class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 11/14/2013 14:41'!
newWith: t1 and: t2 
	
	|instance|
	
	instance := self new.
	^instance initializeWith: t1 and: t2.! !


Transformador subclass: #TransSubstring
	instanceVariableNames: 'from to'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransSubstring methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/11/2013 12:01'!
initializeWith:fromNew and:toNew
	
	from := fromNew.
	to := toNew.
	! !

!TransSubstring methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform:unString 

	^ unString copyReplaceAll:from with:to.

	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

TransSubstring class
	instanceVariableNames: ''!

!TransSubstring class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/11/2013 12:01'!
newWith:from and:to 

	|instance|
	
	instance := self new.
	^instance initializeWith:from and:to.! !


TransCompuesto subclass: #TransSecuencial
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransSecuencial methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform: unObjeto

   ^ t2 transform: (t1 transform: unObjeto).! !


Transformador subclass: #TransCaracter
	instanceVariableNames: 'from to'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransCaracter methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/11/2013 12:01'!
initializeWith:fromNew and:toNew
	
	from := fromNew.
	to := toNew.
	! !

!TransCaracter methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform:unString 

	^ unString replaceAll: from with: to.

	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

TransCaracter class
	instanceVariableNames: ''!

!TransCaracter class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/11/2013 12:01'!
newWith:from and:to 

	|instance|
	
	instance := self new.
	^instance initializeWith:from and:to.! !


Transformador subclass: #TransIdentidad
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransIdentidad methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform: unObjeto
	^unObjeto.! !


Transformador subclass: #TransBloque
	instanceVariableNames: 'amount'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransBloque methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 11/11/2013 16:53'!
initializeWith:newAmount

	amount := newAmount.! !

!TransBloque methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform:unBloque

	^ [ amount timesRepeat: unBloque]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

TransBloque class
	instanceVariableNames: ''!

!TransBloque class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 11/11/2013 16:52'!
newWith:amount

	|instance|
	
	instance := self new.
	^instance initializeWith:amount.! !


TransCompuesto subclass: #TransCondicion
	instanceVariableNames: 'condition'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransCondicion methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:03'!
initializeWith:aCondition going:newt1 or:newt2

	condition := aCondition.
	self initializeWith: newt1 and: newt2! !

!TransCondicion methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform: unObjeto
	
	(condition value: unObjeto) ifTrue: 
		[^t1 transform: unObjeto]
	ifFalse:
		[^t2 transform: unObjeto]
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

TransCondicion class
	instanceVariableNames: ''!

!TransCondicion class methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 12:58'!
newWith:aCondition going:t1 or:t2 
	
	|instance|
	
	instance := self new.
	^instance initializeWith:aCondition going:t1 or:t2. ! !


TransCompuesto subclass: #TransAleatorio
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PLP-TP3'!

!TransAleatorio methodsFor: 'as yet unclassified' stamp: 'MartinCarreiro 12/10/2013 13:32'!
transform: unObjeto
	|listInstanceVariables aRandomTransformer|
	
	listInstanceVariables := (LinkedList new add:t1 )add: t2.
	aRandomTransformer := listInstanceVariables atRandom.
	
   ^ aRandomTransformer transform: unObjeto.! !
